import { Controller } from 'stimulus';
import Swup from 'swup';
import SwupDebugPlugin from '@swup/debug-plugin';
import SwupFormsPlugin from '@swup/forms-plugin';
import SwupFadeTheme from '@swup/fade-theme';
export default class extends Controller {
  connect() {
    new Swup({
      containers: ['#swup'],
      plugins: [new SwupDebugPlugin(), new SwupFadeTheme(), new SwupFormsPlugin()],
      cache: false
    });
  }

}